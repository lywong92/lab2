/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author ylwong
 *
 */

public class Fahrenheit extends Temperature {
	
	public Fahrenheit(float t) {
		super(t);
		}
	
	public String toString() {
		String temp = Float.toString(this.getValue());
		
		return temp;
		}
	
	@Override
	public Temperature toCelsius() {
		float num = this.getValue();
		num = ((num- 32)*5)/9;
		//System.out.println("new temp is" + num);
		Temperature t = new Celsius(num);
		return t;

	}
	@Override
	public Temperature toFahrenheit() {
		Temperature t = new Fahrenheit(this.getValue());
		return t;
	} 
	}