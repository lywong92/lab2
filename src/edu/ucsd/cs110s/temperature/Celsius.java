/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author ylwong
 *
 */

public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
		}
	public String toString()
	{
		String temp = Float.toString(this.getValue());
		
		return temp;
		}
	
	public Temperature toCelsius() {
		Temperature t = new Celsius(this.getValue());
		return t;
	}
	
	public Temperature toFahrenheit() {
		float num = this.getValue();
		num = ((num*9)/5) + 32;
		//System.out.println("new temp is" + num);
		Temperature t = new Fahrenheit(num);
		return t;
	}
	}
